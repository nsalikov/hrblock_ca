# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import re
from scrapy.exceptions import DropItem


class HrblockCaPipeline(object):
    pattern = re.compile('(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?')

    def process_item(self, item, spider):
        item['Name'] = item['Name'].replace('&#8211; ', ' ')
        item['Address'] = item['Address'].replace('&#8211; ', ' ')

        phones = [list(x) for x in re.findall(self.pattern, item['Phone'].strip())]
        item['Phone'] = ''

        for p in phones:
            if p[0] and not p[1]:
                p[1] = p[0]
                p[0] = ''
            if all([p[1], p[2], p[3]]):
                item['Phone'] = '({}) {}-{}'.format(p[1], p[2], p[3])
                if p[0]:
                    item['Phone'] = p[0] + ' ' + item['Phone']
                if p[4]:
                    item['Phone'] = item['Phone'] + ' ext ' + p[4]

        return item


class DuplicatesPipeline(object):

    def __init__(self):
        self.seen = set()


    def process_item(self, item, spider):
        return self.filter_by_keys(item, spider, ['Name', 'Address', 'Phone'])


    def filter_by_keys(self, item, spider, keys):
        values = [item[key] for key in keys if key in item]
        s = ', '.join(values)

        if s in self.seen:
            raise DropItem("Duplicate {}:".format(s))
        else:
            self.seen.add(s)
            return item
